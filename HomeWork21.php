<?php
$json_str = file_get_contents("PhoneBook.json");
$json_obj = json_decode($json_str,true);


if($json_obj == NULL||count($json_obj) == 0)
{
	echo("файл не содержит данных в нужном формате");
	exit;
}
$arr_header = array_keys($json_obj[0]);

?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>ДЗ 2.1 Установка и настройка веб-сервера</title>
	<style type="text/css">
		div {
			display: table;
		}
		ul {
			display: table-row;
		}
		li {
			display: table-cell;
			padding: 0 5px;
			vertical-align: middle;
			border: 1px solid;
		}
		.header {
			font-weight: 700;
			text-align: center;
		}
	</style>
</head>
<body>
	<div>
		<h2>Телефоны</h2>
		<ul>
			<?php
			foreach ($arr_header as &$header) {	?>
			<li class="header"><?= $header ?></li>
			<?php	} ?>
		</ul>
		<?php
		foreach ($json_obj as &$item) {	
		?>
		<ul>
			<?php	foreach ($arr_header as &$header) {
				if (is_array($item[$header])) { 
			?>
			<li><?= implode(', ',$item[$header])?></li>
			<?php } else {
			?>
				<li><?= $item[$header] ?></li>	
			<?php  }
					} 
			?>
		</ul>
		<?php	}	
		?>		
	</div>
</body>
